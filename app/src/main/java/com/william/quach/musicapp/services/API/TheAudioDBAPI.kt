package com.william.quach.musicapp.services.API

import com.william.quach.musicapp.models.*
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface TheAudioDBAPI {

    @GET("trending.php")
    fun getTopActualSongsAsync(
        @Query("country") country: String = "us",
        @Query("type") type: String = "itunes",
        @Query("format") format: String = "singles"
    ): Deferred<TopSongsNetworkResponse>

    @GET("trending.php")
    fun getTopActualAlbumsAsync(
        @Query("country") country: String = "us",
        @Query("type") type: String = "itunes",
        @Query("format") format: String = "albums"
    ): Deferred<TopAlbumsNetworkResponse>

    @GET("search.php")
    fun searchArtistsAsync(
        @Query("s") searchText: String
    ): Deferred<SearchedArtistsNetworkResponse>

    @GET("searchalbum.php")
    fun searchAlbumsByArtistNameAsync(
        @Query("s") searchText: String
    ): Deferred<SearchedAlbumsNetworkResponse>

    @GET("track-top10.php")
    fun getArtistMostLikedSongsByArtistNameAsync(
        @Query("s") searchText: String
    ): Deferred<ArtistMostLikedSongsNetworkResponse>

    @GET("track.php")
    fun getAlbumSongsByAlbumIdAsync(
        @Query("m") searchText: String
    ): Deferred<AlbumSongsNetworkResponse>

    @GET("album.php")
    fun getAlbumByAlbumIdAsync(
        @Query("m") searchText: String
    ):Deferred<AlbumNetworkResponse>
}