package com.william.quach.musicapp.database

import androidx.room.*
import com.william.quach.musicapp.models.Artist

@Dao
abstract class ArtistDBDao {
    @Insert
    abstract fun addArtistToFavorite(artist: Artist?)
    @Query("SELECT * FROM Artist")
    abstract fun getFavoriteArtists(): List<Artist>
    @Query("SELECT EXISTS(SELECT * FROM Artist WHERE id = :artistId)")
    abstract fun isAFavoriteArtist(artistId: String): Boolean
    @Delete
    abstract fun removeFavoriteArtist(artist: Artist?)
    @Update
    abstract fun updateFavoriteArtist(artist: Artist?)
}