package com.william.quach.musicapp.fragments.interfaces

import android.content.Context
import com.william.quach.musicapp.models.Album

interface CellAlbumClickListener {
    fun onCellClickListener(context: Context, album: Album)
}