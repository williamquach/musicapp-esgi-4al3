package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListSongsAdapter
import com.william.quach.musicapp.fragments.interfaces.CellSongClickListener
import com.william.quach.musicapp.models.Song
import com.william.quach.musicapp.services.API.TheAudioDBNetworkRequest
import kotlinx.android.synthetic.main.content_songs.*
import kotlinx.coroutines.*

class SongsFragment : Fragment(), CellSongClickListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_songs, container, false)
    }

    override fun onStart() {
        super.onStart()
        songs_recycler?.layoutManager = LinearLayoutManager(context)
        updateSongsRecyclerAdapter(listOf())

        GlobalScope.launch(Dispatchers.Main) {
            val songs = withContext(Dispatchers.IO) { TheAudioDBNetworkRequest.getTopActualSongs() }
            updateSongsRecyclerAdapter(songs.trending ?: listOf())
        }
    }

    private fun updateSongsRecyclerAdapter(songs: List<Song>) {
        songs_recycler?.adapter = ListSongsAdapter(requireContext(), songs, this)
    }

    override fun onCellClickListener(context: Context, song: Song) {
        val myIntent = Intent(requireActivity(), SongDetails::class.java)
        myIntent.putExtra("song", song) //Optional parameters
        requireActivity().startActivity(myIntent)
    }
}
