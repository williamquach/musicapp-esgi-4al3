package com.william.quach.musicapp.fragments.interfaces

import android.content.Context
import com.william.quach.musicapp.models.Artist

interface CellSearchedArtistClickListener {
    fun onCellClickListener(context: Context, artist: Artist)
}