package com.william.quach.musicapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationBarView
import com.william.quach.musicapp.fragments.FavoritesFragment
import com.william.quach.musicapp.fragments.RankingFragment
import com.william.quach.musicapp.fragments.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val rankingFragment: Fragment = RankingFragment()
    private val searchFragment: Fragment = SearchFragment()
    private val favoritesFragment: Fragment = FavoritesFragment()
    private val fm: FragmentManager = supportFragmentManager
    private var active: Fragment = rankingFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_ranking)

        navigation.setOnItemSelectedListener(mOnNavigationItemSelectedListener)

        fm.beginTransaction().add(R.id.main_container, favoritesFragment, "3").hide(favoritesFragment).commit()
        fm.beginTransaction().add(R.id.main_container, searchFragment, "2").hide(searchFragment).commit()
        fm.beginTransaction().add(R.id.main_container, rankingFragment, "1").commit()
    }

    private val mOnNavigationItemSelectedListener =
        NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_ranking -> {
                    fm.beginTransaction().hide(active).show(rankingFragment).commit()
                    active = rankingFragment
                    supportActionBar?.title = getString(R.string.title_ranking)
                    return@OnItemSelectedListener true
                }
                R.id.navigation_search -> {
                    fm.beginTransaction().hide(active).show(searchFragment).commit()
                    active = searchFragment
                    supportActionBar?.title = getString(R.string.title_search)
                    return@OnItemSelectedListener true
                }
                R.id.navigation_favorites -> {
                    fm.beginTransaction().hide(active).show(favoritesFragment).commit()
                    active = favoritesFragment
                    supportActionBar?.title = getString(R.string.title_favorites)
                    return@OnItemSelectedListener true
                }
            }
            false
        }
}