package com.william.quach.musicapp.fragments.interfaces

import android.content.Context
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Artist

interface CellSearchedAlbumClickListener {
    fun onCellClickListener(context: Context, album: Album)
}