package com.william.quach.musicapp.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.lang.reflect.Field
import java.util.*


data class TopSongsNetworkResponse(
    @SerializedName("trending")
    val trending: List<Song>?
)

data class AlbumSongsNetworkResponse(
    @SerializedName("track")
    val albumSongs: List<Song>?
)

data class ArtistMostLikedSongsNetworkResponse(
    @SerializedName("track")
    val songs: List<Song>?
)

@Entity
class Song (
    @PrimaryKey
    @SerializedName("idTrack")
    val id: String,
    @SerializedName("idAlbum")
    val idAlbum: String,
    @SerializedName("strTrack")
    val name: String,
    @SerializedName("strArtist")
    val artistName: String,
    @SerializedName("strTrackThumb")
    val imageUrl: String,
    @SerializedName("idLyric")
    val songLyric: String,
) : Serializable {
    override fun toString(): String {
        return "$name - $artistName\n" +
                "$songLyric"
    }
}