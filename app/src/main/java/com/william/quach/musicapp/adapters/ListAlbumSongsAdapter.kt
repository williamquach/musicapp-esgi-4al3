package com.william.quach.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.william.quach.musicapp.R
import com.william.quach.musicapp.fragments.interfaces.CellSongClickListener
import com.william.quach.musicapp.models.Song

class ListAlbumSongsAdapter(
    private val context: Context,
    private val songs: List<Song>,
    private val cellClickListener: CellSongClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListAlbumSongsViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.album_songs_list, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val item = holder as ListAlbumSongsViewHolder
        item.bindPosition(position, songs)

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(context, songs[position])
        }
    }

    override fun getItemCount(): Int {
        return songs.size
    }
}

class ListAlbumSongsViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val songIndex: TextView = v.findViewById(R.id.album_songs_index)
    private val songName: TextView = v.findViewById(R.id.album_songs_name)

    fun bindPosition(position: Int, songs: List<Song>) {
        val song = songs[position]
        songIndex.text = (position + 1).toString()
        songName.text = song.name
    }
}