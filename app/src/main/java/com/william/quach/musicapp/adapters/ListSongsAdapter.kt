package com.william.quach.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.fragments.interfaces.CellSongClickListener
import com.william.quach.musicapp.models.Song

class ListSongsAdapter(
    private val context: Context,
    private val songs: List<Song>,
    private val cellClickListener: CellSongClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListSongViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.songs_list, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val item = holder as ListSongViewHolder
        item.bindPosition(position, songs)

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(context, songs[position])
        }
    }

    override fun getItemCount(): Int {
        return songs.size
    }
}

class ListSongViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val songIndex: TextView = v.findViewById(R.id.song_ranking_index)
    private val songName: TextView = v.findViewById(R.id.song_name)
    private val songImage: ImageView = v.findViewById(R.id.song_image)
    private val songSinger: TextView = v.findViewById(R.id.song_artist)

    fun bindPosition(position: Int, songs: List<Song>) {
        val song = songs[position]
        songIndex.text = (position + 1).toString()
        Picasso.get().load(song.imageUrl).into(songImage)
        songName.text = song.name
        songSinger.text = song.artistName
    }
}