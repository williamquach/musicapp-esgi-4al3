package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListAlbumSongsAdapter
import com.william.quach.musicapp.database.DatabaseManager
import com.william.quach.musicapp.fragments.interfaces.CellSearchedArtistClickListener
import com.william.quach.musicapp.fragments.interfaces.CellSongClickListener
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Song
import com.william.quach.musicapp.models.AlbumSongsNetworkResponse
import com.william.quach.musicapp.models.Artist
import com.william.quach.musicapp.services.API.TheAudioDBNetworkRequest
import kotlinx.android.synthetic.main.album_details.*
import kotlinx.android.synthetic.main.artist_details.like_button
import kotlinx.android.synthetic.main.artist_details.liked_icon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class AlbumDetails : AppCompatActivity(), CellSearchedArtistClickListener, CellSongClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.album_details)

        go_back_btn_album.setOnClickListener { finish() }

        albumDetailsProgressBar.visibility = View.VISIBLE
        setAlbumDetails()
    }

    private fun setAlbumDetails() {
        val intent = intent
        val extras = intent.extras

        if (extras === null || extras.get("album") === null) {
            throw Exception("No album passed to AlbumDetails Activity !!!")
        }
        val albumTmp = extras["album"] as Album
        GlobalScope.launch(Dispatchers.Main) {
           val chosenAlbum = withContext(Dispatchers.IO) {
                TheAudioDBNetworkRequest.getAlbumByAlbumId(albumTmp.id).album?.get(0)
                    ?: albumTmp
            }

            println("$chosenAlbum")
            setAlbumCoverAndElementsInside(chosenAlbum)
            setAlbumDescription(chosenAlbum)
            setAlbumSongs(chosenAlbum)
        }
    }

    private fun setAlbumDescription(chosenAlbum: Album) {
        albumDescription.text =
            chosenAlbum.getDescriptionByLang()
                ?: getString(R.string.no_description)
    }

    private fun setAlbumCoverAndElementsInside(chosenAlbum: Album) {
        album_album_name.text = chosenAlbum.name
        album_artist_name.text = chosenAlbum.artistName
        if(chosenAlbum.scoreVotes != null ) number_of_votes.text = chosenAlbum.scoreVotes + " " + getString(R.string.votes)
        if(chosenAlbum.score != null ) album_score.text = chosenAlbum.score
        GlobalScope.launch(Dispatchers.Main) {
            val numberOfSongs = withContext(Dispatchers.IO) {
                TheAudioDBNetworkRequest.getAlbumSongsByAlbumId(chosenAlbum.id).albumSongs
                    ?: listOf()
            }

            if(numberOfSongs.size <= 1){
                number_of_songs.text = "${numberOfSongs.size} " + getString(R.string.song)
            }else {
                number_of_songs.text = "${numberOfSongs.size} " + getString(R.string.songs)
            }
        }
        if (chosenAlbum.coverUrl == null) {
            album_album_name.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))
            album_album_name.setPadding(15, 120, 0, 0)

            val constraintSet = ConstraintSet()
            constraintSet.clone(album_details_constraint_layout)
            constraintSet.connect(
                R.id.album_artist_name,
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP,
                0
            )
            constraintSet.connect(
                R.id.album_cover,
                ConstraintSet.RIGHT,
                R.id.album_album_name,
                ConstraintSet.LEFT,
                0

            )
            constraintSet.connect(
                R.id.album_album_name,
                ConstraintSet.TOP,
                R.id.album_artist_name,
                ConstraintSet.BOTTOM,
                0
            )
            constraintSet.connect(
                R.id.number_of_songs,
                ConstraintSet.TOP,
                R.id.album_album_name,
                ConstraintSet.BOTTOM,
                0
            )
            constraintSet.applyTo(album_details_constraint_layout)

            number_of_songs.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.black
                )
            )
        } else {
            Picasso.get().load(chosenAlbum.coverUrl).into(album_background)
            Picasso.get().load(chosenAlbum.coverUrl).into(album_cover)
            album_background.setColorFilter(getColor(R.color.black), PorterDuff.Mode.DST_ATOP)
        }


        albumDetailsProgressBar.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val db = DatabaseManager.getInstance(applicationContext)
            val doesUserLikeAlbum = withContext(Dispatchers.IO) {
                db.favoriteAlbumsDAO().isAFavoriteAlbum(chosenAlbum.id)
            }
            liked_icon.visibility = if (doesUserLikeAlbum) View.VISIBLE else View.GONE
        }
        albumDetailsProgressBar.visibility = View.GONE

        like_button.setOnClickListener {
            updateLikeButton(chosenAlbum)
        }
    }

    private fun updateLikeButton(chosenAlbum: Album) {
        GlobalScope.launch(Dispatchers.Main) {
            val db = DatabaseManager.getInstance(applicationContext)
            val doesUserLikeAlbum = withContext(Dispatchers.IO) {
                db.favoriteAlbumsDAO().isAFavoriteAlbum(chosenAlbum.id)
            }

            if (doesUserLikeAlbum) {
                liked_icon.visibility = View.GONE
                withContext(Dispatchers.IO) {
                    db.favoriteAlbumsDAO().removeFavoriteAlbum(chosenAlbum)
                }
            } else {
                liked_icon.visibility = View.VISIBLE
                withContext(Dispatchers.IO) {
                    if(chosenAlbum.scoreVotes == null) chosenAlbum.scoreVotes = "0"
                    if(chosenAlbum.score == null) chosenAlbum.score = "0"
                    db.favoriteAlbumsDAO().addAlbumToFavorite(chosenAlbum)
                }
            }
        }
    }

    private fun setAlbumSongs(album: Album) {
        album_songs?.layoutManager = LinearLayoutManager(applicationContext)
        updateAlbumSongsRecyclerAdapter(listOf())

        albumDetailsProgressBar.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val albumSongs = withContext(Dispatchers.IO) {
                TheAudioDBNetworkRequest.getAlbumSongsByAlbumId(album.id)
            }
            updateAlbumSongsItems(albumSongs)
            albumDetailsProgressBar.visibility = View.GONE
        }
    }

    private fun updateAlbumSongsRecyclerAdapter(songs: List<Song>) {
        album_songs?.adapter =
            ListAlbumSongsAdapter(applicationContext, songs, this)
    }

    private fun updateAlbumSongsItems(albumSongs: AlbumSongsNetworkResponse) {
        val songs = albumSongs.albumSongs ?: listOf()
        no_album_songs_found_label.visibility = if (songs.isEmpty()) View.VISIBLE else View.GONE
        updateAlbumSongsRecyclerAdapter(songs)
    }

    override fun onCellClickListener(context: Context, artist: Artist) {
        val myIntent = Intent(this, ArtistDetails::class.java)
        myIntent.putExtra("artist", artist) //Optional parameters
        this.startActivity(myIntent)
    }

    override fun onCellClickListener(context: Context, song: Song) {
        val myIntent = Intent(this, SongDetails::class.java)
        myIntent.putExtra("song", song) //Optional parameters
        this.startActivity(myIntent)
    }


}