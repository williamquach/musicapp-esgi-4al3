package com.william.quach.musicapp.fragments

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.models.Song
import kotlinx.android.synthetic.main.album_details.*
import kotlinx.android.synthetic.main.song_details.*


class SongDetails : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.song_details)

        go_back_btn_track.setOnClickListener { finish() }

        songDetailsProgressBar.visibility = View.VISIBLE
        setSongDetails()
    }

    private fun setSongDetails() {
        val intent = intent
        val extras = intent.extras

        if (extras === null || extras.get("song") === null) {
            throw Exception("No song passed to SongDetails Activity !!!")
        }
        val chosenSong= extras["song"] as Song

        println("$chosenSong")
        setSongCoverAndElementsInside(chosenSong)
        setLyrics(chosenSong)
    }

    private fun setSongCoverAndElementsInside(chosenSong: Song) {
        track_name.text = chosenSong.name

        if (chosenSong.imageUrl == null) {
            lyrics_title.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))
            track_name.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))
            track_name.setPadding(15, 120, 0, 0)

            val constraintSet = ConstraintSet()
            constraintSet.clone(track_details_constraint_layout)
            constraintSet.connect(
                R.id.lyrics_title,
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP,
                0
            )
            constraintSet.connect(
                R.id.track_cover,
                ConstraintSet.RIGHT,
                R.id.track_name,
                ConstraintSet.LEFT,
                0

            )
            constraintSet.connect(
                R.id.track_name,
                ConstraintSet.TOP,
                R.id.lyrics_title,
                ConstraintSet.BOTTOM,
                0
            )

            constraintSet.applyTo(track_details_constraint_layout)

        } else {
            Picasso.get().load(chosenSong.imageUrl).into(track_background)
            Picasso.get().load(chosenSong.imageUrl).into(track_cover)
            track_background.setColorFilter(getColor(R.color.black), PorterDuff.Mode.DST_ATOP)
        }

        songDetailsProgressBar.visibility = View.VISIBLE
    }

    private fun setLyrics(chosenSong: Song){
        if (chosenSong.songLyric == null) lyrics.text = getString(R.string.no_lyrics)
        else lyrics.text = chosenSong.songLyric
        songDetailsProgressBar.visibility = View.GONE
    }


}