package com.william.quach.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.fragments.interfaces.CellSearchedAlbumClickListener
import com.william.quach.musicapp.models.Album

class ListSearchedAlbumsAdapter(
    private val context: Context,
    private val albums: List<Album>,
    private val cellClickListener: CellSearchedAlbumClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListSearchedAlbumViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.searched_albums_list, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val item = holder as ListSearchedAlbumViewHolder
        item.bindPosition(position, albums)

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(context, albums[position])
        }
    }

    override fun getItemCount(): Int {
        return albums.size
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
}

class ListSearchedAlbumViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val albumName: TextView = v.findViewById(R.id.album_name)
    private val albumArtistName: TextView = v.findViewById(R.id.album_artist_name)
    private val albumPicture: ImageView = v.findViewById(R.id.album_image)

    fun bindPosition(position: Int, albums: List<Album>) {
        val album = albums[position]
        Picasso.get().load(album.coverUrl).into(albumPicture)
        albumName.text = album.name
        albumArtistName.text = album.artistName
    }
}