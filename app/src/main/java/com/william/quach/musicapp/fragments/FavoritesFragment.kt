package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListSearchedAlbumsAdapter
import com.william.quach.musicapp.adapters.ListSearchedArtistsAdapter
import com.william.quach.musicapp.database.DatabaseManager
import com.william.quach.musicapp.fragments.interfaces.CellSearchedAlbumClickListener
import com.william.quach.musicapp.fragments.interfaces.CellSearchedArtistClickListener
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Artist
import kotlinx.android.synthetic.main.content_favorites.*
import kotlinx.coroutines.*

class FavoritesFragment : Fragment(), CellSearchedArtistClickListener, CellSearchedAlbumClickListener {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_favorites, container, false)
    }

    override fun onStart() {
        super.onStart()
        val databaseManager = context?.let { DatabaseManager.getInstance(it) }
        if (databaseManager === null) {
            throw Error("Room Database manager cannot be null!")
        }
        val favoriteArtistsDAO = databaseManager.favoriteArtistsDAO()
        val favoriteAlbumsDAO = databaseManager.favoriteAlbumsDAO()

        favorite_artists_recycler?.layoutManager = LinearLayoutManager(context)
        updateFavoriteArtistsRecyclerAdapter(listOf())
        favorite_albums_recycler?.layoutManager = LinearLayoutManager(context)
        updateFavoriteAlbumsRecyclerAdapter(listOf())

        favoriteProgressBar.visibility= View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val favoriteArtists = withContext(Dispatchers.IO) {
                favoriteArtistsDAO.getFavoriteArtists()
            }
            updateSearchedArtistsItems(favoriteArtists)

            val favoriteAlbums = withContext(Dispatchers.IO) {
                favoriteAlbumsDAO.getFavoriteAlbums()
            }
            updateFavoriteAlbumsItems(favoriteAlbums)

            favoriteProgressBar.visibility = View.GONE
        }
    }

    private fun updateSearchedArtistsItems(favoriteArtists: List<Artist>) {
        noFavoriteArtistFoundLabel.visibility = if (favoriteArtists.isEmpty()) View.VISIBLE else View.GONE
        updateFavoriteArtistsRecyclerAdapter(favoriteArtists)
    }

    private fun updateFavoriteArtistsRecyclerAdapter(artists: List<Artist>) {
        favorite_artists_recycler.adapter = ListSearchedArtistsAdapter(requireContext(), artists, this)
    }

    override fun onCellClickListener(context: Context, artist: Artist) {
        val myIntent = Intent(requireActivity(), ArtistDetails::class.java)
        myIntent.putExtra("artist", artist) //Optional parameters
        requireActivity().startActivity(myIntent)
    }

    private fun updateFavoriteAlbumsItems(favoriteAlbums: List<Album>) {
        noFavoriteAlbumLabel.visibility = if (favoriteAlbums.isEmpty()) View.VISIBLE else View.GONE
        updateFavoriteAlbumsRecyclerAdapter(favoriteAlbums)
    }

    private fun updateFavoriteAlbumsRecyclerAdapter(albums: List<Album>) {
        favorite_albums_recycler.adapter = ListSearchedAlbumsAdapter(requireContext(), albums, this)
    }

    override fun onCellClickListener(context: Context, album: Album) {
        val myIntent = Intent(requireActivity(), AlbumDetails::class.java)
        myIntent.putExtra("album", album) //Optional parameters
        requireActivity().startActivity(myIntent)
    }
}