package com.william.quach.musicapp.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.lang.reflect.Field
import java.util.*


data class TopAlbumsNetworkResponse(
    @SerializedName("trending")
    val trending: List<Album>?
)

data class SearchedAlbumsNetworkResponse(
    @SerializedName("album")
    val albums: List<Album>?
)

data class AlbumNetworkResponse(
    @SerializedName("album")
    val album: List<Album>?
)

@Entity
class Album (
    @PrimaryKey
    @SerializedName("idAlbum")
    val id: String,
    @SerializedName("strAlbum")
    val name: String,
    @SerializedName("strArtist")
    val artistName: String,
    @SerializedName("strAlbumThumb")
    val coverUrl: String,
    @SerializedName("intScoreVotes")
    var scoreVotes: String,
    @SerializedName("intScore")
    var score: String,
    @SerializedName("strDescriptionEN")
    val strDescriptionEN: String?,
    val strDescriptionUS: String? = strDescriptionEN,
    @SerializedName("strDescriptionDE")
    val strDescriptionDE: String?,
    @SerializedName("strDescriptionFR")
    val strDescriptionFR: String?,
    @SerializedName("strDescriptionCN")
    val strDescriptionCN: String?,
    @SerializedName("strDescriptionIT")
    val strDescriptionIT: String?,
    @SerializedName("strDescriptionJP")
    val strDescriptionJP: String?,
    @SerializedName("strDescriptionRU")
    val strDescriptionRU: String?,
    @SerializedName("strDescriptionES")
    val strDescriptionES: String?,
    @SerializedName("strDescriptionPT")
    val strDescriptionPT: String?,
    @SerializedName("strDescriptionSE")
    val strDescriptionSE: String?,
    @SerializedName("strDescriptionNL")
    val strDescriptionNL: String?,
    @SerializedName("strDescriptionHU")
    val strDescriptionHU: String?,
    @SerializedName("strDescriptionNO")
    val strDescriptionNO: String?,
    @SerializedName("strDescriptionIL")
    val strDescriptionIL: String?,
    @SerializedName("strDescriptionPL")
    val strDescriptionPL: String?,
    val numberOfSongs: Int,

    ) : Serializable {
    fun getDescriptionByLang(): String? {
        val descriptionByUserLang: Field =
            Album::class.java.getDeclaredField("strDescription${Locale.getDefault().country}")
        return descriptionByUserLang.get(this)?.toString() ?: strDescriptionEN
    }



    override fun toString(): String {
        return "$name - $artistName\n" +
                "Description : ${this.getDescriptionByLang()}"  +
                "Cover url : $coverUrl\n" +
                "Score : $score\n" +
                "Score votes : $scoreVotes"
    }


}