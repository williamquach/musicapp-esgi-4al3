package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListAlbumsAdapter
import com.william.quach.musicapp.fragments.interfaces.CellAlbumClickListener
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.services.API.TheAudioDBNetworkRequest
import kotlinx.android.synthetic.main.content_albums.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AlbumsFragment : Fragment(), CellAlbumClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_albums, container, false)
    }

    override fun onStart() {
        super.onStart()

        albums_recycler?.layoutManager = LinearLayoutManager(context)
        updateAlbumsRecyclerAdapter(listOf())

        GlobalScope.launch(Dispatchers.Main) {
            val albums = withContext(Dispatchers.IO) { TheAudioDBNetworkRequest.getTopActualAlbums() }
            updateAlbumsRecyclerAdapter(albums.trending ?: listOf())
        }
    }

    private fun updateAlbumsRecyclerAdapter(albums: List<Album>) {
        albums_recycler?.adapter = ListAlbumsAdapter(requireContext(), albums, this)
    }

    override fun onCellClickListener(context: Context, album: Album) {
        val myIntent = Intent(requireActivity(), AlbumDetails::class.java)
        myIntent.putExtra("album", album) //Optional parameters
        requireActivity().startActivity(myIntent)
    }
}