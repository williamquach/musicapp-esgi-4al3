package com.william.quach.musicapp.fragments.interfaces

import android.content.Context
import com.william.quach.musicapp.models.Song

interface CellSongClickListener {
    fun onCellClickListener(context: Context, song: Song)
}