package com.william.quach.musicapp.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.lang.reflect.Field
import java.util.*


data class SearchedArtistsNetworkResponse(
    @SerializedName("artists")
    val artists: List<Artist>?
)

@Entity
class Artist(
    @PrimaryKey
    @SerializedName("idArtist")
    val id: String,
    @SerializedName("strArtist")
    val name: String,
    @SerializedName("strArtistThumb")
    val imageUrl: String?,
    @SerializedName("strArtistFanart")
    val fanArtUrl: String?,
    @SerializedName("strCountry")
    val country: String?,
    @SerializedName("strGenre")
    val genre: String?,
    @SerializedName("strBiographyEN")
    val strBiographyEN: String?,
    @SerializedName("strBiographyDE")
    val strBiographyDE: String?,
    @SerializedName("strBiographyFR")
    val strBiographyFR: String?,
    @SerializedName("strBiographyCN")
    val strBiographyCN: String?,
    @SerializedName("strBiographyIT")
    val strBiographyIT: String?,
    @SerializedName("strBiographyJP")
    val strBiographyJP: String?,
    @SerializedName("strBiographyRU")
    val strBiographyRU: String?,
    @SerializedName("strBiographyES")
    val strBiographyES: String?,
    @SerializedName("strBiographyPT")
    val strBiographyPT: String?,
    @SerializedName("strBiographySE")
    val strBiographySE: String?,
    @SerializedName("strBiographyNL")
    val strBiographyNL: String?,
    @SerializedName("strBiographyHU")
    val strBiographyHU: String?,
    @SerializedName("strBiographyNO")
    val strBiographyNO: String?,
    @SerializedName("strBiographyIL")
    val strBiographyIL: String?,
    @SerializedName("strBiographyPL")
    val strBiographyPL: String?,
) : Serializable {
    fun getCountryAndGenre(): String? {
        return if (country != null && country != "") {
            return if (genre != null && genre != "") "$country - $genre" else ""
        } else genre
    }

    fun getBiographyByLang(): String? {
        val biographyByUserLang: Field =
            Artist::class.java.getDeclaredField("strBiography${Locale.getDefault().country}")
        return biographyByUserLang.get(this)?.toString() ?: strBiographyEN
    }

    override fun toString(): String {
        return "$name - $country - $genre\n" +
                "Bio : $strBiographyEN\n" +
                "Fan Art url : $fanArtUrl\n" +
                "Image url : $imageUrl"
    }
}