package com.william.quach.musicapp.database

import android.content.Context

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Artist
import com.william.quach.musicapp.models.Song

@Database(entities = [Artist::class, Album::class, Song::class], version = 3)
abstract class DatabaseManager : RoomDatabase() {

    companion object {

        @Volatile
        private var INSTANCE: DatabaseManager? = null

        fun getInstance(context: Context): DatabaseManager =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DatabaseManager::class.java, "db.sqlite"
            )
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun favoriteArtistsDAO(): ArtistDBDao
    abstract fun favoriteAlbumsDAO(): AlbumDBDao
}
