package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListSearchedAlbumsAdapter
import com.william.quach.musicapp.adapters.ListSearchedArtistsAdapter
import com.william.quach.musicapp.fragments.interfaces.CellSearchedAlbumClickListener
import com.william.quach.musicapp.fragments.interfaces.CellSearchedArtistClickListener
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Artist
import com.william.quach.musicapp.models.SearchedAlbumsNetworkResponse
import com.william.quach.musicapp.models.SearchedArtistsNetworkResponse
import com.william.quach.musicapp.services.API.TheAudioDBNetworkRequest
import kotlinx.android.synthetic.main.content_search.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SearchFragment : Fragment(), CellSearchedArtistClickListener, CellSearchedAlbumClickListener {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.content_search, container, false)
        return view
    }

    // TODO: Utiliser RecyclerView avec la particularité de gérer plusieurs types de ViewHolder.
    // Pour cela, il faut implémenter la méthode getItemViewType dans l’Adapter et fournir la
    // bonne vue dans le onCreateViewHolder (via l’attribut viewType).
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searched_artists_recycler?.layoutManager = LinearLayoutManager(context)
        updateSearchedArtistsRecyclerAdapter(listOf())

        searched_albums_recycler?.layoutManager = LinearLayoutManager(context)
        updateSearchedAlbumsRecyclerAdapter(listOf())

        searchTextField.editText?.doOnTextChanged { text, start, before, count ->
            searchProgressBar.visibility= View.VISIBLE

            GlobalScope.launch(Dispatchers.Main) {
                val searchedArtists = withContext(Dispatchers.IO) { TheAudioDBNetworkRequest.searchArtists(
                    "$text"
                ) }
                updateSearchedArtistsItems(searchedArtists)

                val searchedAlbums = withContext(Dispatchers.IO) { TheAudioDBNetworkRequest.searchAlbumsByArtistName(
                    "$text"
                ) }
                updateSearchedAlbumsItems(searchedAlbums)

                searchProgressBar.visibility = View.GONE
            }
        }
    }

    private fun updateSearchedArtistsItems(searchedArtists: SearchedArtistsNetworkResponse) {
        val artists = searchedArtists.artists ?: listOf()
        noArtistFoundLabel.visibility = if (artists.isEmpty()) View.VISIBLE else View.GONE
        updateSearchedArtistsRecyclerAdapter(artists)
    }

    private fun updateSearchedArtistsRecyclerAdapter(artists: List<Artist>) {
        searched_artists_recycler?.adapter = ListSearchedArtistsAdapter(requireContext(), artists, this)
    }

    private fun updateSearchedAlbumsItems(searchedAlbums: SearchedAlbumsNetworkResponse) {
        val albums = searchedAlbums.albums ?: listOf()
        noAlbumFoundLabel.visibility = if (albums.isEmpty()) View.VISIBLE else View.GONE
        updateSearchedAlbumsRecyclerAdapter(albums)
    }

    private fun updateSearchedAlbumsRecyclerAdapter(albums: List<Album>) {
        searched_albums_recycler?.adapter = ListSearchedAlbumsAdapter(requireContext(), albums, this)
    }

    override fun onCellClickListener(context: Context, artist: Artist) {
        val myIntent = Intent(requireActivity(), ArtistDetails::class.java)
        myIntent.putExtra("artist", artist) //Optional parameters
        requireActivity().startActivity(myIntent)
    }

    override fun onCellClickListener(context: Context, album: Album) {
        val myIntent = Intent(requireActivity(), AlbumDetails::class.java)
        myIntent.putExtra("album", album) //Optional parameters
        requireActivity().startActivity(myIntent)
    }
}