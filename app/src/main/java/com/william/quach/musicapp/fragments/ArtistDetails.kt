package com.william.quach.musicapp.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.adapters.ListMostLikedSongsAdapter
import com.william.quach.musicapp.adapters.ListSearchedAlbumsAdapter
import com.william.quach.musicapp.database.DatabaseManager
import com.william.quach.musicapp.fragments.interfaces.CellSearchedAlbumClickListener
import com.william.quach.musicapp.fragments.interfaces.CellSongClickListener
import com.william.quach.musicapp.models.*
import com.william.quach.musicapp.services.API.TheAudioDBNetworkRequest
import kotlinx.android.synthetic.main.artist_details.*
import kotlinx.coroutines.*


class ArtistDetails : AppCompatActivity(), CellSearchedAlbumClickListener, CellSongClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.artist_details)

        go_back_btn.setOnClickListener { finish() }

        artistDetailsProgressBar.visibility = View.VISIBLE
        setArtistDetails()
    }

    private fun setArtistDetails() {
        val intent = intent
        val extras = intent.extras

        if (extras === null || extras.get("artist") === null) {
            throw Exception("No artist passed to ArtistDetails Activity !!!")
        }
        val chosenArtist = extras["artist"] as Artist

        setArtistFanArtAndElementsInside(chosenArtist)
        setArtistAlbums(chosenArtist)
        setArtistDescription(chosenArtist)
        setArtistMostLikedSongs(chosenArtist)
    }

    private fun setArtistDescription(chosenArtist: Artist) {
        artistDescription.text =
            chosenArtist.getBiographyByLang()
                ?: getString(R.string.no_description)
    }

    private fun setArtistFanArtAndElementsInside(chosenArtist: Artist) {
        artistName.text = chosenArtist.name
        artist_origins_and_music_genre.text = chosenArtist.getCountryAndGenre()

        if (chosenArtist.fanArtUrl == null) {
            artistName.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))
            artistName.setPadding(15, 120, 0, 0)

            val constraintSet = ConstraintSet()
            constraintSet.clone(artist_details_constraint_layout)
            constraintSet.connect(
                R.id.artistName,
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP,
                0
            )
            constraintSet.connect(
                R.id.artist_origins_and_music_genre,
                ConstraintSet.TOP,
                R.id.artistName,
                ConstraintSet.BOTTOM,
                0
            )
            constraintSet.applyTo(artist_details_constraint_layout)

            artist_origins_and_music_genre.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.black
                )
            )
        } else {
            Picasso.get().load(chosenArtist.fanArtUrl).into(artist_fan_art)
        }


        artistDetailsProgressBar.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val db = DatabaseManager.getInstance(applicationContext)
            val doesUserLikeArtist = withContext(Dispatchers.IO) {
                db.favoriteArtistsDAO().isAFavoriteArtist(chosenArtist.id)
            }
            liked_icon.visibility = if (doesUserLikeArtist) View.VISIBLE else View.GONE

            if (doesUserLikeArtist) {
                val databaseManager = DatabaseManager.getInstance(applicationContext)
                withContext(Dispatchers.IO) {
                    databaseManager.favoriteArtistsDAO().updateFavoriteArtist(chosenArtist)
                }
            }
            artistDetailsProgressBar.visibility = View.GONE
        }

        like_button.setOnClickListener {
            updateLikeButton(chosenArtist)
        }
    }

    private fun updateLikeButton(chosenArtist: Artist) {
        GlobalScope.launch(Dispatchers.Main) {
            val db = DatabaseManager.getInstance(applicationContext)
            val doesUserLikeArtist = withContext(Dispatchers.IO) {
                db.favoriteArtistsDAO().isAFavoriteArtist(chosenArtist.id)
            }

            if (doesUserLikeArtist) {
                liked_icon.visibility = View.GONE
                withContext(Dispatchers.IO) {
                    db.favoriteArtistsDAO().removeFavoriteArtist(chosenArtist)
                }
            } else {
                liked_icon.visibility = View.VISIBLE
                withContext(Dispatchers.IO) {
                    db.favoriteArtistsDAO().addArtistToFavorite(chosenArtist)
                }
            }
        }
    }

    private fun setArtistAlbums(artist: Artist) {
        artist_albums?.layoutManager = LinearLayoutManager(applicationContext)
        updateArtistAlbumsAlbumsRecyclerAdapter(listOf())

        albums_title.text = getString(R.string.artist_albums, "")
        artistDetailsProgressBar.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val albums = withContext(Dispatchers.IO) {
                TheAudioDBNetworkRequest.searchAlbumsByArtistName(artist.name)
            }
            updateArtistAlbumsItems(albums)
            albums_title.text = getString(R.string.artist_albums, "(${albums.albums?.size})")
            artistDetailsProgressBar.visibility = View.GONE
        }
    }

    private fun updateArtistAlbumsItems(searchedAlbums: SearchedAlbumsNetworkResponse) {
        val albums = searchedAlbums.albums ?: listOf()
        no_artist_album_found_label.visibility = if (albums.isEmpty()) View.VISIBLE else View.GONE
        updateArtistAlbumsAlbumsRecyclerAdapter(albums)
    }

    private fun updateArtistAlbumsAlbumsRecyclerAdapter(albums: List<Album>) {
        artist_albums?.adapter = ListSearchedAlbumsAdapter(applicationContext, albums, this)
    }


    private fun setArtistMostLikedSongs(artist: Artist) {
        artist_most_liked_songs?.layoutManager = LinearLayoutManager(applicationContext)
        updateArtistMostLikedSongsRecyclerAdapter(listOf())

        artistDetailsProgressBar.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            val mostLikedSongs = withContext(Dispatchers.IO) {
                TheAudioDBNetworkRequest.getArtistMostLikedSongsByArtistName(artist.name)
            }
            updateArtistMostLikedSongsItems(mostLikedSongs)
            artistDetailsProgressBar.visibility = View.GONE
        }
    }

    private fun updateArtistMostLikedSongsRecyclerAdapter(songs: List<Song>) {
        artist_most_liked_songs?.adapter =
            ListMostLikedSongsAdapter(applicationContext, songs, this)
    }

    private fun updateArtistMostLikedSongsItems(mostLikedSongs: ArtistMostLikedSongsNetworkResponse) {
        val songs = mostLikedSongs.songs ?: listOf()
        no_artist_songs_found_label.visibility = if (songs.isEmpty()) View.VISIBLE else View.GONE
        updateArtistMostLikedSongsRecyclerAdapter(songs)
    }

    override fun onCellClickListener(context: Context, album: Album) {
        val myIntent = Intent(this, AlbumDetails::class.java)
        myIntent.putExtra("album", album) //Optional parameters
        this.startActivity(myIntent)
    }

    override fun onCellClickListener(context: Context, song: Song) {
        val myIntent = Intent(this, SongDetails::class.java)
        myIntent.putExtra("song", song) //Optional parameters
        this.startActivity(myIntent)
    }
}