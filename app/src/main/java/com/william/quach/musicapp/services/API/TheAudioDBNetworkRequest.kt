package com.william.quach.musicapp.services.API

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.william.quach.musicapp.models.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object TheAudioDBNetworkRequest {

    private val api = Retrofit.Builder()
        .baseUrl("https://theaudiodb.com/api/v1/json/523532/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(TheAudioDBAPI::class.java)

    suspend fun getTopActualSongs() : TopSongsNetworkResponse {
        return api.getTopActualSongsAsync().await()
    }

    suspend fun getTopActualAlbums() : TopAlbumsNetworkResponse {
        return api.getTopActualAlbumsAsync().await()
    }

    suspend fun searchArtists(toSearch: String) : SearchedArtistsNetworkResponse {
        return api.searchArtistsAsync(toSearch).await()
    }

    suspend fun searchAlbumsByArtistName(artistName: String) : SearchedAlbumsNetworkResponse {
        return api.searchAlbumsByArtistNameAsync(artistName).await()
    }

    suspend fun getArtistMostLikedSongsByArtistName(artistName: String) : ArtistMostLikedSongsNetworkResponse {
        return api.getArtistMostLikedSongsByArtistNameAsync(artistName).await()
    }

    suspend fun getAlbumSongsByAlbumId(albumId: String) : AlbumSongsNetworkResponse {
        return api.getAlbumSongsByAlbumIdAsync(albumId).await()
    }

    suspend fun getAlbumByAlbumId(albumId: String) : AlbumNetworkResponse {
        return api.getAlbumByAlbumIdAsync(albumId).await()
    }

}