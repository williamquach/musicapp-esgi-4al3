package com.william.quach.musicapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.tabs.TabLayout
import com.william.quach.musicapp.R
import kotlinx.android.synthetic.main.content_ranking.*

class RankingFragment : Fragment() {
    private val songsFragment: Fragment = SongsFragment()
    private val albumsFragment: Fragment = AlbumsFragment()
    private var active: Fragment = songsFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_ranking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fm: FragmentManager = childFragmentManager

        fm.beginTransaction().add(R.id.tab_layout_container, albumsFragment, "1").hide(albumsFragment).commit()
        fm.beginTransaction().add(R.id.tab_layout_container, songsFragment, "2").commit()

        ranking_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                // Handle tab select
                when (tab!!.text) {
                    getString(R.string.tab_albums) -> {
                        fm.beginTransaction().hide(active).show(albumsFragment).commit()
                        active = albumsFragment
                    }
                    getString(R.string.tab_songs) -> {
                        fm.beginTransaction().hide(active).show(songsFragment).commit()
                        active = songsFragment
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Handle tab unselect
            }
        })
    }
}