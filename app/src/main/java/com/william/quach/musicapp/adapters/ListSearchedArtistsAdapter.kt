package com.william.quach.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.R
import com.william.quach.musicapp.fragments.interfaces.CellSearchedArtistClickListener
import com.william.quach.musicapp.models.Artist

class ListSearchedArtistsAdapter(
    private val context: Context,
    private val artists: List<Artist>,
    private val cellClickListener: CellSearchedArtistClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListSearchedArtistViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.searched_artists_list, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val item = holder as ListSearchedArtistViewHolder
        item.bindPosition(position, artists)

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(context, artists[position])
        }
    }

    override fun getItemCount(): Int {
        return artists.size
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
}

class ListSearchedArtistViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val artistName: TextView = v.findViewById(R.id.artist_name)
    private val artistPicture: ImageView = v.findViewById(R.id.artist_image)

    fun bindPosition(position: Int, artists: List<Artist>) {
        val artist = artists[position]
        Picasso.get().load(artist.imageUrl).into(artistPicture)
        artistName.text = artist.name
    }
}