package com.william.quach.musicapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.william.quach.musicapp.fragments.interfaces.CellAlbumClickListener
import com.william.quach.musicapp.R
import com.william.quach.musicapp.models.Album

open class ListAlbumsAdapter(
    private val context: Context,
    private val albums: List<Album>,
    private val cellClickListener: CellAlbumClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListAlbumViewHolder(
            LayoutInflater
                .from(context)
                .inflate(R.layout.albums_list, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        val item = holder as ListAlbumViewHolder
        item.bindPosition(position, albums)

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(context, albums[position])
        }
    }

    override fun getItemCount(): Int {
        return albums.size
    }
}

class ListAlbumViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val albumIndex: TextView = v.findViewById(R.id.album_ranking_index)
    private val albumName: TextView = v.findViewById(R.id.album_name)
    private val albumImage: ImageView = v.findViewById(R.id.album_image)
    private val albumSinger: TextView = v.findViewById(R.id.album_artist_name)

    fun bindPosition(position: Int, albums: List<Album>) {
        val album = albums[position]
        albumIndex.text = (position + 1).toString()
        Picasso.get().load(album.coverUrl).into(albumImage)
        albumName.text = album.name
        albumSinger.text = album.artistName
    }
}