package com.william.quach.musicapp.database

import androidx.room.*
import com.william.quach.musicapp.models.Album
import com.william.quach.musicapp.models.Song

@Dao
abstract class AlbumDBDao {
    @Insert
    abstract fun addAlbumToFavorite(album: Album?)
    @Query("SELECT * FROM Album")
    abstract fun getFavoriteAlbums(): List<Album>
    @Query("SELECT EXISTS(SELECT * FROM Album WHERE id = :albumId)")
    abstract fun isAFavoriteAlbum(albumId: String): Boolean
    @Query("SELECT COUNT(*) FROM Song WHERE idAlbum = :albumId")
    abstract fun getNumberOfAlbumSongs(albumId: String): String
    @Delete
    abstract fun removeFavoriteAlbum(album: Album?)
    @Update
    abstract fun updateFavoriteAlbum(album: Album?)
}